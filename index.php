<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>AustralCoin</title>
    <link rel="stylesheet" href="assets/css/estilo.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Baloo+Tammudu+2:wght@400;500;600;700;800&display=swap" rel="stylesheet">
</head>
<body>
    <!--header - top-->
   <header>
       <div class="header-content">
           <div class="logo">
               <h1>Austral<b>COIN</b></h1>
           </div>
           <div class="menu">
               <nav>
                   <ul>
                       <li><a href="">Inicio</a></li>
                       <li><a href="">Ayudanos</a></li>
                       <li><a href="">Proyecto</a></li>
                       <li><a href="">Contacto</a></li>
                   </ul>
               </nav>
           </div>
       </div>
   </header>
   <!--portada-->
   <div class="container-cover">
       <div class="container-info-cover">
           <h1>
               La moneda de tu futuro
           </h1>
           <p>
            Sera la moneda estable, encriptada y decentralizada
           </p>
       </div>
   </div>
   <!--Login Register-->
   <div class="container-login-register">
       <div class="login-box">
           <form action="" class="formulario-login">
               <h2>Iniciar Sesión</h2>
               <input type="text" placeholder="Correo Electronico">
               <input type="password" placeholder="Contrasena">
               <button>Entrar</button>
           </form>
           <form action="php/registro_usuario_be.php" method="POST" class="formulario-register">
               <h2>Registrarse</h2>
               <input type="text" placeholder="Nombre Completo" name="nombre_completo">
               <input type="text" placeholder="Correo Electronico" name="correo">
               <input type="text" placeholder="Usuario" name="usuario">
               <input type="password" placeholder="Contrasena" name="contrasena">
               <button>Registrarse</button>
           </form>
       </div>
   </div>
</body>
</html>